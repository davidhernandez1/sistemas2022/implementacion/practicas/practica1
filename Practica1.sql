﻿DROP DATABASE IF EXISTS m1u2p1;
CREATE DATABASE IF NOT EXISTS m1u2p1;
USE m1u2p1;
CREATE TABLE clientes
(
cod int AUTO_INCREMENT,
nombre varchar(100),
PRIMARY KEY(cod)
);
CREATE TABLE coches
(
id int AUTO_INCREMENT,
`cod-cliente` int,
marca varchar(100),
fecha date,
precio float,
PRIMARY KEY(id),
CONSTRAINT fkcochesclientes
FOREIGN KEY (`cod-cliente`)
REFERENCES clientes(cod)
ON UPDATE RESTRICT
ON DELETE RESTRICT
);

INSERT INTO clientes (cod, nombre)
  VALUES (1, 'pepe'),
         (2,'juan'),
         (3,'pedro');
INSERT INTO coches (id, `cod-cliente`, marca, fecha, precio)
  VALUES (1, 1, 'Ferrari', '2002/11/7', 500000),
         (2, 1, 'Masseratti', '2002/12/7', 250000);

SELECT * FROM clientes c;
SELECT * FROM coches co;